//
//  AppDelegate.swift
//  Memder
//
//  Created by Maxime POULAIN on 07/02/2023.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow()
        let navigation = UINavigationController()
        let mainView = MainController(nibName: nil, bundle: nil)
        navigation.viewControllers = [mainView]
        self.window!.rootViewController = navigation
        self.window?.makeKeyAndVisible()
        return true
    }
}

