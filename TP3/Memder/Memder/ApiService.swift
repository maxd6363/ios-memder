//
//  ApiService.swift
//  Memder
//
//  Created by Maxime POULAIN on 07/02/2023.
//

import UIKit

class ApiService {
    
    let subreddit = ["rance", "memes", "genshin_memepact", "wowcomics", "hmmm", "cosplay", "nier", "OldSchoolCool", "animemes", "ProgrammerHumor", "rienabranler", "ZeldaBotW" ]
    var selectedSubreddit = "rance"
    
    func getMemes(completion: @escaping (Result<Memes, Error>) -> Void) {
        let url = URL(string: "https://meme-api.com/gimme/\(selectedSubreddit)/50")!
        let task = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let data = data else {
                completion(.failure(NSError(domain: "", code: 0)))
                return
            }
            do {
                let memes = try JSONDecoder().decode(Memes.self, from: data);
                completion(.success(memes))
                    
            } catch {
                print("Error decoding JSON: \(error)")
            }
        }
        task.resume()
    }
    
}
