//
//  Main.swift
//  Memder
//
//  Created by Maxime POULAIN on 07/02/2023.
//

import UIKit

class MainController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func clickList(_ sender: Any) {
        self.navigationController?.pushViewController(MemeListController(), animated: true)
    }

    @IBAction func clickStart(_ sender: Any) {
        self.navigationController?.pushViewController(VoteController(), animated: true)
    }
    
}
