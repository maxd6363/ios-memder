//
//  VoteController.swift
//  Memder
//
//  Created by Maxime POULAIN on 21/02/2023.
//
import UIKit

class VoteController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    var memes : Memes?
    var service : ApiService?
    var currentImage : Int?
    
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var imageMeme: UIImageView!
    @IBOutlet weak var memeName: UILabel!
    @IBOutlet weak var picker: UIPickerView!
    
    @IBAction func dislikeButton(_ sender: Any) {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeLeft.direction = .left
        
        handleSwipe(send: swipeLeft)
    }
    
    @IBAction func likeButton(_ sender: Any) {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeRight.direction = .right
        
        handleSwipe(send: swipeRight)
    }
    
    @IBAction func undo(_ sender: Any) {
        if(currentImage ?? 0 > 1) {
            self.currentImage? -= 2
            self.nextImage()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.dataSource = self
        picker.delegate = self
        undoButton.isHidden = true
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeLeft.direction = .left
        view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeRight.direction = .right
        view.addGestureRecognizer(swipeRight)
        
        service = ApiService()
        reload()
        
    }
    
    func reload(){
        service?.getMemes {(result) in
            switch result{
            case .success(let memes):
                self.memes = memes
                self.currentImage = 0
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    self?.spinner.isHidden = true
                    self?.spinner.stopAnimating()
                }
                self.nextImage()

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func nextImage() {
        
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            self?.undoButton.isHidden = self?.currentImage ?? 0 < 2
        }
        
        if(currentImage ?? 0 < memes?.memes.count ?? 0) {
            
            let meme = memes?.memes[currentImage ?? 0]
            self.imageMeme.load(url: URL(string: meme?.url ?? "")!)
            
            let title = meme?.title ?? ""
            
            DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
                self!.memeName.text = title
            }
            self.currentImage! += 1
        }
        else {
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @objc func handleSwipe(send: UISwipeGestureRecognizer) {
        UIView.animate(withDuration:0.3) {
            if send.direction == .left {
                let translate = CGAffineTransform(translationX: -150, y: 20)
                self.imageMeme.transform = translate.concatenating(CGAffineTransform(rotationAngle: -CGFloat.pi / 8))
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                    self?.nextImage()
                    self?.imageMeme.transform = CGAffineTransform.identity
                }
            }
            else if send.direction == .right {
                let translate = CGAffineTransform(translationX: 150, y: 20)
                self.imageMeme.transform = translate.concatenating(CGAffineTransform(rotationAngle: CGFloat.pi / 8))
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                    self?.nextImage()
                    self?.imageMeme.transform = CGAffineTransform.identity
                }
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return service?.subreddit.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return service?.subreddit[row] ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        service?.selectedSubreddit = service?.subreddit[row] ?? "rance"
        reload()
    }
    
}
