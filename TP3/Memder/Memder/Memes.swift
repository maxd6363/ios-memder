//
//  Memes.swift
//  Memder
//
//  Created by Maxime POULAIN on 07/02/2023.
//

struct Memes: Codable {
    var count: Int = 0;
    var memes: [Meme] = [];
}
