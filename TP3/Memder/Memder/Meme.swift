//
//  Meme.swift
//  Memder
//
//  Created by Maxime POULAIN on 07/02/2023.
//

struct Meme : Codable {
    var title: String;
    var url: String;
    var author: String;
}
