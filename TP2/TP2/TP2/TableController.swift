//
//  TableController.swift
//  TP2
//
//  Created by Maxime POULAIN on 31/01/2023.
//

import UIKit


class TableController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    

    var regions = Regions.all
            
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        tableView.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return regions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = regions[indexPath.row].nom
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsView = DetailsController()
        detailsView.region = regions[indexPath.row]
        
        self.navigationController?.pushViewController(detailsView, animated: true)
    }
}


