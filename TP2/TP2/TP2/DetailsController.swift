//
//  DetailsController.swift
//  TP2
//
//  Created by Maxime POULAIN on 31/01/2023.
//

import UIKit

class DetailsController: UIViewController {
    
    var region: Region?
    
    @IBOutlet weak var Details: UILabel!
    
    @IBOutlet weak var President: UILabel!
    
    @IBOutlet weak var Population: UILabel!
    
    @IBOutlet weak var Superficie: UILabel!
    
    @IBOutlet weak var Image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Details.text = region?.nom
        President.text! += region!.president
        Population.text! += "\(region!.population)"
        Superficie.text! += "\(region!.superficie)"
        if(region?.image != nil){
            Image.load(url: URL(string: region?.image ?? "")!)
        }
        
        
    }

    
}

extension UIImageView {
    func load(url: URL){
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url){
                if let image = UIImage(data: data){
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
