//
//  AppDelegate.swift
//  TP2
//
//  Created by Maxime POULAIN on 31/01/2023.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow()
        let navigation = UINavigationController()
        let mainView = TableController(nibName: nil, bundle: nil)
        navigation.viewControllers = [mainView]
        self.window!.rootViewController = navigation
        self.window?.makeKeyAndVisible()
        return true
    }

}

