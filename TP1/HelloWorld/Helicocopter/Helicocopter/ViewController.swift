//
//  ViewController.swift
//  Helicocopter
//
//  Created by Maxime POULAIN on 24/01/2023.
//

import UIKit

class ViewController: UIViewController {
    
    var nb_tirages = 0

    @IBOutlet var Background: UIView!
    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var ColorWell: UIColorWell!
    
    @IBAction func Click(_ sender: Any) {
        if nb_tirages == 6 {
            nb_tirages = 0
            self.Label.text = ""
        }
        else if nb_tirages == 5 {
            self.Label.text! += "\n \(Int.random(in: 1..<11))"
            nb_tirages += 1
        }
        else {
            self.Label.text! += " \(Int.random(in: 1..<100))"
            nb_tirages += 1
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Label.text = ""
        ColorWell.addTarget(self, action: #selector(colorChanged), for: .valueChanged)
        
    }
    
    @objc func colorChanged(){
        Background.backgroundColor = ColorWell.selectedColor ?? .blue
    }

}

